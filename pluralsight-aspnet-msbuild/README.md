# pluralsight-aspnet-msbuild

Contains notes from the Pluralsight podcast

[ASP.NET Core with the New MSBuild Based Tooling](https://app.pluralsight.com/library/courses/aspdotnet-core-msbuild-tooling/table-of-contents)

## Notes

[Create projects](./notes/02-create-projects/notes.md)

[Manage projects](./notes/03-manage-projects/notes.md)

[Build and publish](./notes/04-build-and-publish/notes.md)


