# Pluralsight: Asp.Net Core MSBuild Tooling: Manage Projects

## Multiproject: Create projects with nested structure

We can create folder with multiple projects in it using both the dotnet CLI or the Visual Studio.
Having a solution file helps to reference together all the projects.
Besides that, the project files may reference each other:

![multi project example](multi-project-example_2017-06-25_13h23_51.png)

### Multiproject: Create web Project

Let's first create the MVC web project:

    dotnet new mvc -o Weather\Web

![created the web project](dotnet-new-mvc-weather-project_2017-06-25_13h29_17.png)

Move into the `Weather` folder:

    cd weather

### Multiproject: Add library Project

Next step, let's create the library project. What are the options to create library project? Running:

    dotnet new -h

tells us there is a template for a class library `classlib`:

![dotnet new -h](dotnet-new-h_2017-06-25_13h35_17.png)

If we want to see if there are some options for template `classlib`, then we can run:

    dotnet new classlib -h

![dotnet new classlib -h](dotnet-new-classlib-h_2017-06-25_13h36_15.png)

We have the typical framework option. If we want to have a wider audience for our code, then we want to target `netstandard` framework.

To understand better the different choices, look at the [dotnet standard](https://github.com/dotnet/standard) GitHub repository and in [docs/versions.md](https://github.com/dotnet/standard/blob/master/docs/versions.md) there is a  table that explains the dependencies:

![dotnet standard versions table](dotnet-standard-versions-table_2017-06-25_13h44_10.png)

When choosing a .NET Standard version, you should consider this trade-off:

- The higher the version, the more APIs are available to you.

- The lower the version, the more platforms implement it.

Let's use the default framework `netstandard1.4` and create the `Client` library project:

    dotnet new classlib -o Client

And this is the folder structure and the content of the `Client.csproj` file:

![client library project created](client-project-created_2017-06-25_13h48_53.png)

### Multiproject: Add library test Project

If we run again:

    dotnet new -h

we see 2 different unit test project templates `mstest` and `xunit`.

As for the `xunit`, if we run:

    dotnet new xunit -h

the options we have are only for the framework. We want to have the same framework as the mvc project and it was by default `netcoreapp1.1`, we can always confirm with:

    dotnet new mvc -h

, or look at the project file:

    cat .\Web\Web.csproj
    ...
    <PropertyGroup>
        <TargetFramework>netcoreapp1.1</TargetFramework>
    </PropertyGroup>

We then create the `xunit` project:

    dotnet new xunit -o Client.Tests

This is now what we have as directories:

![dir all three projects created](dir-all-three-projects-2017-06-25_18h49_53.png)

The content of the test project directory is:

![test project directory](dir-test-project_2017-06-25_18h50_38.png)

And this is how the project file of the `Client.Tests` looks like:

![content of the test file](test-project-file_2017-06-25_18h51_53.png)

### Multiproject: Add Solution File

As we are in the main `Weather` directory we can just run:

    dotnet new sln

and it will create `Weather.sln` file for us:

![solution file](Initial-content-of-solution-file_2017-06-25_18h59_05.png)

Now how do we add the projects to the solution. We can run:

    dotnet -h

, and we see there is a `sln` command. Then we can see the options:

    dotnet sln -h

![dotnet sln command help](dotnet-sln-help-command_2017-06-25_19h01_45.png)

we can skip the solution file name, if in he current directory is only one solution it will use it. We add all the projects to the solution:

    dotnet sln add .\Web\Web.csproj
    dotnet sln add .\Client\Client.csproj
    dotnet sln add .\Client.Tests\Client.Tests.csproj

And then we can list all the projects included in the solution with:

    dotnet sln list

![list projects in solution](list-projects-in-solution_2017-06-25_19h12_13.png)

### Multiproject: Project to Project References

The project references are implemented using `ProjectReference` element in a separate `ItemGroup`, just as the references to the Nuget packages:

![project file project reference](project-file-project-reference_2017-06-25_19h17_49.png)

We use the  `dotnet add` command.

First we add reference from Client.Tests.csproj:

    cd .\Client.Tests\
    dotnet add reference ..\Client\Client.csproj
    cd ..

Then we add reference from the Web.csproj:

    cd .\Web\
    dotnet add reference ..\Client\Client.csproj
    cd ..

we can now confirm the references are created with:

    dotnet list .\Web\Web.csproj reference
    dotnet list .\Client.Tests\Client.Tests.csproj reference

![dotnet list project references](dotnet-list-project-references_2017-06-25_19h27_17.png)

### Multiproject: Add Nuget packages to project

We want to add `FluentAssertions` and `Moq` libraries to the `Client.Tests` project. They both have as dependencies `netstandard1.3`, if we look at the `nuget.org`:

    cd .\Client.Tests\
    dotnet add package FluentAssertions
    dotnet add package Moq
    cd ..

If we try to install incompatible with our target framework package we get an error:

    cd .\Client.Tests\
    dotnet add package log4net --version 2.0.5
    ...
    error: Package log4net 2.0.5 is not compatible with netcoreapp1.1 ...


### Multiproject: Remove Nuget packages to project

To remove package from csproj project we run `dotnet remove`. Example:

    dotnet remove package FluentAssertions

## Links

[dotnet CLI help](https://docs.microsoft.com/en-us/dotnet/core/tools/)

[dotnet new help](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-new)

[dotnet sln help](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-sln)

... etc.

