# Pluralsight: Asp.Net Core MSBuild Tooling: Create Projects

## dotnet CLI

### dotnet CLI: Create new project

Get help on create new projects:

    dotnet new -h

Build new empty web project:

    dotnet new web -o myweb
    cd myweb
    dotnet restore
    dotnet run

### dotnet CLI: Target specific framework

Get help about the template instantiation commands for `dotnet` CLI:

    dotnet new web -h

It will list how to set different framework versions:

![list of frameworks](dotnet-list-frameworks-2017-06-24_20h52_57.png)

Example command:

    dotnet new mvc --framework netcoreapp1.0 --auth Individual -o mymvcwithauth

If we look at the created csproj file we will find more references and it is using local SQLite database for authentication:

![csproj file mvc with auth](csproj-example-mvcwithauth_2017-06-24_20h57_07.png)

### dotnet CLI: Project names

By default the `dotnet` CLI creates csproj file same name as the folder of the project. Renaming can be a problem as it also controls the namespacing. Below are examples for naming csproj projects.

Not setting the location, creates project in the current directory:

![csproj current directory](csproj-filename-current-directory_2017-06-24_21h05_37.png)

Using `-n` to name the project, it creates directory with the same name:

![name param](csproj-name-parameter_2017-06-24_21h09_41.png)

To give a different name of the csproj file than the directory name, combine `-n` and `-o`:

![specific folder with custom project name](csproj-example-custom-name-specific-folder_2017-06-24_21h14_00.png)

## Nuget

### Nuget: Config files

The `NuGet.config` file stores user defined package sources, credentials and proxy settings.

You can find the global Nuget configuration file on Windows here:

    %AppData%\NuGet\NuGet.Config

You can find the machine wide NuGet configuration under:

    %ProgramData%\NuGet\Config

### Nuget: Packages

To add Nuget package, add sub-element `PackageReference` to the element `ItemGroup`:

### Nuget: Restore

Nuget puts the restored packages under:

    %userprofile%\.nuget\packages

![add nuget package](nuget-add-package_2017-06-24_18h40_32.png)

## csproj Project files

### csproj Project files: Multi-targeting

To target multiple platforms edit your `.csproj` file and add semicolon separated list of targeted platforms:

    <Project Sdk="Microsoft.NET.Sdk.Web">
        <PropertyGroup>
            <TargetFramework>netcoreapp1.1</TargetFramework>
        </PropertyGroup>

like for example:

    <Project Sdk="Microsoft.NET.Sdk.Web">
        <PropertyGroup>
            <TargetFrameworks>netcoreapp1.1;netcoreapp1.0;net452</TargetFrameworks>
        </PropertyGroup>

IMPORTANT: Change the name from `TargetFramework` to `TargetFrameworks`.

After that, run:

    dotnet restore
    dotnet build

, and it will show output similar to:

    myweb -> D:\Projects\Learn\dotnet.core\pluralsight-aspnet-msbuild\myweb\bin\Debug\netcoreapp1.1\myweb.dll
    myweb -> D:\Projects\Learn\dotnet.core\pluralsight-aspnet-msbuild\myweb\bin\Debug\netcoreapp1.0\myweb.dll
    myweb -> D:\Projects\Learn\dotnet.core\pluralsight-aspnet-msbuild\myweb\bin\Debug\net452\myweb.exe

Note: we get `myweb.exe` in the above for the full framework instead of a dll.

### csproj Project files: Project SDK

We include `<Project Sdk=..."`:

    Microsoft.NET.Sdk - apps 
    Microsoft.NET.Sdk.Web - web apps

Looking at the SDK folder at `%ProgramFiles%\dotnet\sdk`

    %ProgramFiles%\dotnet\sdk\1.0.4\Sdks

where you will find the `props` and `targets` files.

This is an example what gets included when publishing a web project:

![publish web project](msbuild-sdk-props_2017-06-24_20h11_24.png)

## Links

[dotnet/templating](https://github.com/dotnet/templating)

[csproj reference](https://docs.microsoft.com/en-us/dotnet/core/tools/csproj)

[dotnet/sdk](https://github.com/dotnet/sdk)

[aspnet/sdk](https://github.com/aspnet/websdk)

[Microsoft/msbuild](https://github.com/microsoft/msbuild)

[NuGet File Locations](http://lastexitcode.com/projects/NuGet/FileLocations/)

[Configuring the behavior of NuGet](https://docs.microsoft.com/en-us/nuget/consume-packages/configuring-nuget-behavior)

[project.json and csproj comparison and mapping](https://docs.microsoft.com/en-us/dotnet/core/tools/project-json-to-csproj)



